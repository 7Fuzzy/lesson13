#pragma comment(lib, "ws2_32.lib")
#include "Server.h"
#include "WSAInitializer.h"


int main()
{
	WSAInitializer wsainit;
	
	try
	{
		Server s = Server("data.txt");
		s.Listen();
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}

	getchar();
	return 0;
}