#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "Server.h"
#include <exception>
#include "Helper.h"
#include <mutex>
#include <thread>

#define NAME_OFF 5
#define DATA_OFF 8


mutex msgLock;
mutex usrLock;
condition_variable conMsg;

Server::Server(string dataPath)
{
	_serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSock == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__  " - socket");
	}

	this->_dataPath = dataPath;
	fstream f;
	if (!experimental::filesystem::exists(this->_dataPath))
	{
		f.open(this->_dataPath, fstream::out);
		f.close();
	}
}


Server::~Server()
{
	try
	{
		closesocket(_serverSock);
	}
	catch (...) {}
}

void Server::GetMessages(SOCKET clnt)
{
	while (true)
	{
		string msg = "";
		int type = 0;
		try
		{
			type = Helper::getMessageTypeCode(clnt);
			int size = 0;
			switch (type)
			{
			case MT_CLIENT_LOG_IN:
				size = Helper::getIntPartFromSocket(clnt, 2);
				msg = to_string(type) + Helper::getPaddedNumber(size, 2) + Helper::getStringPartFromSocket(clnt, size);
				usrLock.lock();
				this->_users.push_back(pair<string, SOCKET>(msg.substr(NAME_OFF), clnt));
				usrLock.unlock();
				this->UpdateAllClients();
				break;
			case MT_CLIENT_UPDATE:
			case MT_CLIENT_FINISH:
				size = Helper::getIntPartFromSocket(clnt, 5);
				msg = to_string(type) + Helper::getPaddedNumber(size, 5) + Helper::getStringPartFromSocket(clnt, size);
				break;
			case MT_CLIENT_EXIT:
				msg = to_string(type);
				break;
			}
		}
		catch (exception)
		{
			closesocket(clnt);
			break;
		}
		if (msg != "")
		{
			if (type != MT_CLIENT_LOG_IN)
			{
				unique_lock<mutex> l(msgLock);
				this->_messages.push(msg);
				l.unlock();
				conMsg.notify_all();
			}
		}
	}
}

void Server::Update(string msg)
{
	string data = msg.substr(DATA_OFF);
	
	fstream f;
	f.open(this->_dataPath, fstream::out);
	f << data;
	f.close();
}


void Server::HandleMessages()
{
	while (true)
	{
		unique_lock<mutex> l(msgLock);
		conMsg.wait(l, [=]() {return !this->_messages.empty(); });
		string msg = this->_messages.front();
		this->_messages.pop();
		l.unlock();

		string type = msg.substr(0, 3);
		usrLock.lock();
		string curr = this->_users.front().first;
		usrLock.unlock();

		cout << "Curr: " << curr << " Type: " << type << " Msg: " << msg << endl;
		if (type == to_string(MT_CLIENT_UPDATE))
		{
			//cout << curr << "Updated the file\n";
			this->Update(msg);
			this->UpdateAllClients();
		}
		else if (type == to_string(MT_CLIENT_FINISH))
		{
			//cout << curr << " Finished updating the file\n";
			this->Update(msg);
			usrLock.lock();
			pair<string, SOCKET> u = this->_users.front();
			this->_users.pop_front();
			this->_users.push_back(u);
			usrLock.unlock();
			this->UpdateAllClients();
		}
		else if (type == to_string(MT_CLIENT_EXIT))
		{
			//cout << curr << " Disconnected\n";
			this->_users.pop_front();
			this->UpdateAllClients();
		}
		else
		{
			cout << "Invalid Message from client: " << this->_users.front().first << ":" << msg << "\n";
		}
		cout << endl << endl;
	}
}

void Server::UpdateAllClients()
{
	if (this->_users.size() != 0)
	{
		unique_lock<mutex> l(usrLock);
		pair<string, SOCKET> curr = this->_users.front();
		this->_users.pop_front();
		pair<string, SOCKET> next;
		if (!this->_users.empty())
		{
			next = this->_users.front();
		}
		this->_users.push_front(curr);
		l.unlock();

		fstream f;
		f.open(this->_dataPath, fstream::in);
		f.seekg(0, f.end);
		int length = f.tellg();
		f.seekg(0, f.beg);

		char * buffer = new char[length + 1]{ 0 };

		f.read(buffer, length);
		f.close();

		string data(buffer);

		//cout << "File: " << data << '\n';

		l.lock();
		for (int i = 0; i < this->_users.size(); i++)
		{
			Helper::sendUpdateMessageToClient(this->_users[i].second, data,
				curr.first, this->_users.size() != 1 ? next.first : "00", i + 1);
		}
		l.unlock();
	}
}



void Server::Listen()
{
	struct sockaddr_in sa = { 0 };
	int port = 8465;

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = inet_addr("127.0.0.1");    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(this->_serverSock, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		cout << "Error: " << err << endl;
		throw std::exception(__FUNCTION__ " - listen");
	}
	
	// Start listening for incoming requests of clients
	if (listen(this->_serverSock, SOMAXCONN) == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		cout << "Error: " << err << endl;
		throw std::exception(__FUNCTION__ " - listen");
	}
	cout << "Listening on port " << port << endl;

	thread handle(&Server::HandleMessages, this);
	handle.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		//std::cout << "Waiting for client connection request" << std::endl;
		SOCKET connected = ::accept(this->_serverSock, NULL, NULL);
		//cout << " Client Connected!" << endl;
		thread clnt = thread(&Server::GetMessages, this, connected);
		clnt.detach();
	}
}
