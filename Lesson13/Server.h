#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <queue>
#include <iostream>
#include <string>
#include <deque>
#include <fstream>
#include <experimental\filesystem>


using namespace std;

class Server
{
private:
	SOCKET _serverSock;
	queue<string> _messages;
	deque<pair<string, SOCKET>> _users;
	string _dataPath;

	void UpdateAllClients();
	void HandleMessages();
	void GetMessages(SOCKET clnt);
	void Update(string msg);
public:
	Server(string dataPath);
	~Server();
	
	void Listen();

};

